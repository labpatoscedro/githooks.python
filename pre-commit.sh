#!/bin/bash

python3 -m venv env
source ./env/bin/activate
python -m pip install pylint

files=$(ls -d */ | grep -vE '^(env|test)')
pylint -j 4 $files

exit $?